LittleHorse = {}
LittleHorse.name = "LittleHorse"

function LittleHorse:RestorePosition()
    local left = self.savedVariables.left
    local top = self.savedVariables.top

    LittleHorseIndicator:ClearAnchors()
    LittleHorseIndicator:SetAnchor(TOPLEFT, GuiRoot, TOPLEFT, left, top)
end

function LittleHorse.OnIndicatorMoveStop()
    LittleHorse.savedVariables.left = LittleHorseIndicator:GetLeft()
    LittleHorse.savedVariables.top = LittleHorseIndicator:GetTop()
end

function LittleHorse.OnMouseEnter()
    LittleHorseIndicatorLabel:SetHidden(false)
end

function LittleHorse.OnMouseExit()
    LittleHorseIndicatorLabel:SetHidden(true)
end

function LittleHorse:FeedTimer()
    local time, total = GetTimeUntilCanBeTrained()
    local feedMessage = '...'

    if (time ~= nil) then
        if (time == 0) then
            local inv, maxInv, sta, maxSta, speed, maxSpeed = GetRidingStats()
            if inv ~= maxInv or sta ~= maxSta or speed ~= maxSpeed then
                feedMessage = 'FEED'
                LittleHorseIndicatorLabel:SetColor(255, 0, 0)
                LittleHorseIndicatorIcon:SetColor(255, 0, 0)
                ZO_Alert(UI_ALERT_CATEGORY_ALERT, SOUNDS.DEFAULT_CLICK, "Your mount looks hungry.")
            else
                -- TODO This needs tested
                LittleHorseIndicator:SetHidden(true)
                CHAT_SYSTEM:AddMessage("Congratulations, mount fully trained! LittleHorse unloaded.")
                EVENT_MANAGER:UnregisterForUpdate("feedUpdate")
                EVENT_MANAGER:UnregisterEvent(LittleHorse.name, EVENT_STABLE_INTERACT_END)
            end
        elseif (time > 0) then
            local hours = math.floor(time / 3600000)
            local minutes = math.floor((time - (hours * 3600000)) / 60000)
            if (minutes > 9) then
                feedMessage = string.format('%d:%d', hours, minutes)
            else
                feedMessage = string.format('%d:0%d', hours, minutes)
            end
            LittleHorseIndicatorIcon:SetColor(255, 255, 255)
            LittleHorseIndicatorLabel:SetColor(255, 255, 255)
            LittleHorseIndicatorLabel:SetHidden(true)
        end
    end

    LittleHorseIndicatorLabel:SetText(feedMessage)
end

function LittleHorse.GetStats()
    local inv, maxInv, sta, maxSta, speed, maxSpeed = GetRidingStats()
    d("Mount Stats: inventory "..inv.."/"..maxInv
            ..", stamina "..sta.."/"..maxSta
            ..", speed "..speed.."/"..maxSpeed)
end

function LittleHorse.SlashCommand(args)
    if args == "stats" then
        LittleHorse.GetStats()
    else
        d("LittleHorse: type /lh stats to see mount training data.")
    end

end

function LittleHorse:Initialize()
    self.savedVariables = ZO_SavedVars:New("LittleHorseSavedVariables", 1, nil, {})
    self:RestorePosition()
    LittleHorse:FeedTimer()

    EVENT_MANAGER:RegisterForUpdate("feedUpdate", 60000, function()
        LittleHorse:FeedTimer()
    end)

    EVENT_MANAGER:RegisterForEvent(LittleHorse.name, EVENT_STABLE_INTERACT_END, function()
        LittleHorse:FeedTimer()
    end)
end

function LittleHorse.OnAddOnLoad(event, addonName)
    if addonName == LittleHorse.name then
        local horseFragment = ZO_SimpleSceneFragment:New(LittleHorseIndicator)
        HUD_SCENE:AddFragment(horseFragment)
        HUD_UI_SCENE:AddFragment(horseFragment)
        LittleHorse:Initialize()
    end
end

SLASH_COMMANDS["/lh"] = LittleHorse.SlashCommand
EVENT_MANAGER:RegisterForEvent(LittleHorse.name, EVENT_ADD_ON_LOADED, LittleHorse.OnAddOnLoad)